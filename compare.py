import AO3
import json
from datetime import date, datetime
import time
from deepdiff import DeepDiff 
import re
from pprint import pprint

#fn to remove fandom etc
dont_compare = ["fandom", "rating", "warnings", "series", "wordcount"]
def delete_from_dict(dict_del, lst_keys):
    for k in lst_keys:
        try:
            del dict_del[k]
        except KeyError:
            pass
    for v in dict_del.values():
        if isinstance(v, dict):
            delete_from_dict(v, lst_keys)

    return dict_del

#intro
print("hello!")
with open('D:\\Dropbox\\ao3stats\\ao3stats\\stats.txt') as loadfile:
    data = json.load(loadfile)
    dates = list(data.keys())
print("pick a start date!")
print("date range is:")
for date in dates:
      print(date)
#get dates
datadate1 = data[str(datetime.strptime(input(), "%Y-%m-%d").date())]
delete_from_dict(datadate1, dont_compare)
print("pick a finish date!")
datadate2 = data[str(datetime.strptime(input(), "%Y-%m-%d").date())]
delete_from_dict(datadate2, dont_compare)
#compare the dates
if datadate1 == datadate2:
    print("there has been no change!")
else: 
    difference = DeepDiff(datadate1, datadate2, verbose_level=2)
#    pprint(difference, indent = 2)
#if they are not the same
    if len(difference['values_changed']) == 1:
        print("the only difference is the time!")
    else:
        print("difference between " + difference['values_changed']["root['time']"]['old_value'] + " and " + difference['values_changed']["root['time']"]['new_value'] + ":")
        del difference['values_changed']["root['time']"]
        for key in difference['values_changed'].keys():
            workid = int(re.search('\d+', key).group())
            title = datadate2[f"{workid}"]['title']
            print(f"{title}:")
            stattype = re.findall("[a-z]+", key)[1]
            print(stattype + ": " + str(int(difference['values_changed'][key]['new_value']) - int(difference['values_changed'][key]['old_value'])))
        for key in difference['dictionary_item_added'].keys():
            workid = int(re.search('\d+', key).group())
            title = datadate2[f"{workid}"]['title']
            print(f"{title} (new):")
            for attribute in difference['dictionary_item_added'][key].keys():
                if attribute != "title":
                    print(attribute + ": " + str(difference['dictionary_item_added'][key][attribute]))
print("all done :)")
