import AO3
import json
from datetime import date, datetime
import time
print("hi there!")
print("what's your username?")
username = input()
print("and your password, please?")
password = input()
user = AO3.User(username)
start = time.time() # start the clock
session = AO3.Session(username, password)
user.set_session(session)
myworks = user.get_works()
myworks.reverse() # chrono order
print(f"works fetched! there are {len(myworks)} works by {username}")
today = str(date.today())
print(f"collecting data for {today}...")
with open('D:\\Dropbox\\ao3stats\\ao3stats\\stats.txt') as loadfile:
    data = json.load(loadfile)
    data[today] = {}
    data[today]['time'] = str(datetime.now())
    keys = ['title', 'fandom', 'rating', 'warnings', 'hits', 'kudos', 'bookmarks', 'comments', 'wordcount', 'chapters', 'series']
    for i in myworks:
      workid = i.id
      work = AO3.Work(workid, session)
      data[today][workid] = {}.fromkeys(keys)
      data[today][workid]['title'] = work.title
      data[today][workid]['fandom'] = work.fandoms
      data[today][workid]['rating'] = work.rating
      data[today][workid]['warnings'] = work.warnings
      data[today][workid]['hits'] = work.hits
      data[today][workid]['kudos'] = work.kudos
      data[today][workid]['bookmarks'] = work.bookmarks
      data[today][workid]['comments'] = work.comments
      data[today][workid]['wordcount'] = work.words
      data[today][workid]['chapters'] = work.nchapters
      if len(work.series) == 0:
        data[today][workid]['series'] = False
      else:
        data[today][workid]['series'] = True
      print(f"stats recorded for work id {workid}")
with open('D:\\Dropbox\\ao3stats\\ao3stats\\stats.txt', "w") as outfile:
  json.dump(data, outfile)
print(f"all done! processed {len(myworks)} works in {round(time.time()-start, 1)} seconds :)")
print(f"{len(data)} days of stats recorded!")
## print(json.dumps(data, indent=4))

## TO DO
## later: learn how to scrape stats page for private bookmark/comment thread numbers
